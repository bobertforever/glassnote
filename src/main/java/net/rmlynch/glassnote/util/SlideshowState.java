/*
 * Copyright (C) 2014 Robert Lynch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.rmlynch.glassnote.util;

public class SlideshowState {
    private int current_slide;
    private boolean is_playing;

    public boolean isPlaying() {
        return is_playing;
    }

    public int getCurrentSlide() {
        return current_slide;
    }

    public void setCurrent_slide(int current_slide) {
        this.current_slide = current_slide;
    }

    public void setIs_playing(boolean is_playing) {
        this.is_playing = is_playing;
    }
}
