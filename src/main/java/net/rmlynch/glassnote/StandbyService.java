/*
 * Copyright (C) 2014 Robert Lynch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.rmlynch.glassnote;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.google.android.glass.timeline.LiveCard;

public class StandbyService extends Service {
    private String cardID = "GlassnoteLiveCard";
    private LiveCard mLiveCard;

    String address;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        address = intent.getStringExtra("SCAN_RESULT");
        if(mLiveCard == null) {
            // Create the live card
            mLiveCard = new LiveCard(this.getApplicationContext(), cardID);
            RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.layout.card_layout);
            remoteViews.setTextViewText(R.id.text_view, "Connected to " + address);
            mLiveCard.setViews(remoteViews);

            // Create the menu
            Intent menuIntent = new Intent(this, StandbyMenuActivity.class);
            menuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mLiveCard.setAction(PendingIntent.getActivity(this, 0, menuIntent, 0));

            // Publish the live card
            mLiveCard.publish(LiveCard.PublishMode.REVEAL);
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // Remove the live card if it exists
        if (mLiveCard != null && mLiveCard.isPublished()) {
            mLiveCard.unpublish();
            mLiveCard = null;
        }
        super.onDestroy();
    }

    private final StandbyBinder mBinder = new StandbyBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class StandbyBinder extends Binder {
        public String getAddress() {
            return address;
        }
    }
}
