/*
 * Copyright (C) 2014 Robert Lynch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.rmlynch.glassnote;

import net.rmlynch.glassnote.util.Note;
import net.rmlynch.glassnote.util.Notes;
import net.rmlynch.glassnote.util.SlideState;
import net.rmlynch.glassnote.util.SlideshowState;

import retrofit.http.*;

public interface KaasService {
    @GET("/json/start")
    SlideState startSlideshow();

    @GET("/json/pause")
    SlideState pauseSlideshow();

    @GET("/json/next")
    SlideState nextSlide();

    @GET("/json/previous")
    SlideState previousSlide();

    @GET("/json/current_state")
    SlideState getCurrentSlide();

    @GET("/json/monitor")
    SlideshowState slideshowStatus();

    @GET("/json/notes")
    Notes presentationNotes();

    @GET("/json/notes/{id}")
    Note slideNotes(@Path("id") int id);
}