/*
 * Copyright (C) 2014 Robert Lynch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.rmlynch.glassnote;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.AdapterView;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollView;

import net.rmlynch.glassnote.util.Notes;
import net.rmlynch.glassnote.util.SlideState;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;

public class NoteActivity extends Activity {

    private Notes notes;
    private List<Card> mCards;
    private String address;
    private CardScrollView mCardScrollView;
    private NotesScrollAdapter adapter;
    private KaasService kaasService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Bind the service to get the server address
        bindService(new Intent(this, StandbyService.class), mConnection, 0);

        // Create the rest adapter
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(address).build();
        kaasService = restAdapter.create(KaasService.class);

        Thread thread = new Thread()
        {
            @Override
            public void run() {
                // Start the presentation
                notes = kaasService.presentationNotes();
                createCards();
                if (!kaasService.slideshowStatus().isPlaying())
                    kaasService.startSlideshow();
            }
        };

        thread.start();

        // Set the scroll adapter and view
        mCardScrollView = new CardScrollView(this);
        adapter = new NotesScrollAdapter(mCards);
        mCardScrollView.setAdapter(adapter);
        mCardScrollView.setOnItemSelectedListener(mListener);
        mCardScrollView.activate();
        mCardScrollView.setKeepScreenOn(true);
        setContentView(mCardScrollView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (kaasService.slideshowStatus().isPlaying())
            kaasService.pauseSlideshow();
        stopService(new Intent(this, StandbyService.class));
    }

    private void createCards() {
        mCards = new ArrayList<Card>();

        Card temp;
        for(int i = 0; i < notes.getNotes().size(); i++) {
            temp = new Card(this);
            temp.setText(notes.getNotes().get(i).getContent());
            temp.setFootnote("Slide " + (i+1) + " of " + notes.getNotes().size());
            mCards.add(temp);
        }
    }

    private AdapterView.OnItemSelectedListener mListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            SlideState currentSlide = kaasService.getCurrentSlide();
            while (currentSlide.getSlide() != (int) l) {
                if (currentSlide.getSlide() > (int) l)
                    currentSlide = kaasService.previousSlide();
                else
                    currentSlide = kaasService.nextSlide();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //noop
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            // Get the address from the service binder
            if (iBinder instanceof StandbyService.StandbyBinder) {
                address = ((StandbyService.StandbyBinder) iBinder).getAddress();
            }
            unbindService(this);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            //noop
        }
    };
}
