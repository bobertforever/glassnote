GlassNote
===========

A presentation teleprompter and remote for Google Glass

Installing
----------
Install the [latest apk](https://github.com/BobertForever/Glassnote/releases) using adb

    adb install -r Glassnote.apk

To start the app, say "OK Glass, give a presentation" from the Glass clock screen or use the touch menu

Usage
-----
1. The first thing you need to do is [install](https://github.com/BobertForever/GlassNote#installing) the APK using the instructions above.
2. Once installed, download [KAAS](https://github.com/BobertForever/kaas)
3. Start the KeyNote presentation, and then start KAAS (directions can be found [here](https://github.com/BobertForever/kaas#basic-use----command-line))
4. Generate a QR code of the server's address (e.x. http://192.168.1.5:8000)
5. Open GlassNote and scan the generated QR code
6. Start the presentation from the livecard menu and control using the touchpad

App flow
--------
1. Start app
2. Main activity starts QR code scanner
3. Scanner detects a valid QR code and starts the Stand-by service, which generates a livecard
4. User starts the presentation from the menu attached to the livecard
3. NoteActivity is started
  * Activity gets the scanned address from the service
  * Retrofit is created for the KAAS endpoint using the scanned address
  * HTTP request is made to get the presentation notes, which are generated into cards
  * HTTP request is made to start the presentation if it is not already running
4. NoteActivity  takes control of view, and publishes new timeline using generated cards from presentation notes
  * On card change, a HTTP request is made to KAAS to change the slide
6. Presentation app exits when app is closed (swipe-down)
  * HTTP request is made to stop the presentation

Helping out
-----------
This is my first Android App, so I'm looking for any feedback and feature requets. Use the github [issue tracker](https://github.com/BobertForever/Glassnote/issues) to suggest any changes, and pull requests are more then welcome.

Want to help me out and buy me a coffee? Support me via gittip!

[![Support via Gittip](https://rawgithub.com/twolfson/gittip-badge/0.1.0/dist/gittip.png)](https://www.gittip.com/BobertForever/)
